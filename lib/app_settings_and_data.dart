import 'dart:ui';

import 'package:followme/quiz_question.dart';

import 'months.dart';



class SettingsAndData {
  static List<List<QuizQuestion>> questionData = [
//    LvlOneQuestions.questionBank,
//    LvlTwoQuestions.questionBank,
//    LvlThreeQuestions.questionBank,
//    LvlFourQuestions.questionBank,
//    LvlFiveQuestions.questionBank,
//    LvlSixQuestions.questionBank,
//    LvlSevenQuestions.questionBank,
//    LvlEightQuestions.questionBank,
//    LvlNineQuestions.questionBank,
//    LvlTenQuestions.questionBank,

  ];

  static List<Months> months = [
    Months("January",[
      "December 30–January 5","January 6–12","January 13–19","January 20–26"],[
      "Introductory Pages of the Book of Mormon","1 Nephi 1–7","1 Nephi 8–10","1 Nephi 11–15"]),
    Months("February",[
      "January 27–February 2","February 3–9","February 10–16","February 17–23"],[
      "1 Nephi 16–22", "2 Nephi 1–5", "2 Nephi 6–10", "2 Nephi 11–25"
    ]),
    Months("March",[
      "February 24–March 1","March 2–8","March 9–15","March 16–22","March 23–29"],[
      "2 Nephi 26–30", "2 Nephi 31–33", "Jacob 1–4", "Jacob 5–7","Enos–Words of Mormon"
    ]),
    Months("April",[
      "March 30–April 12","April 13–19","April 20–26"],[
      "Easter","Mosiah 1–3","Mosiah 4–6"
    ]),
    Months("May",[
      "April 27–May 3","May 4–10","May 11–17","May 18–24","May 25–31"],[
      "Mosiah 7–10","Mosiah 11–17","Mosiah 18–24","Mosiah 25–28","Mosiah 29–Alma 4"
    ]),
    Months("June",[
      "June 1–7","June 8–14","June 15–21","June 22–28"],[
      "Alma 5–7","Alma 8–12","Alma 13–16","Alma 17–22"
    ]),
    Months("July",[
      "June 29–July 5","July 6–12","July 13–19","July 20–26"],[
      "Alma 23–29","Alma 30–31","Alma 32–35","Alma 36–38"
    ]),
    Months("August",[
      "July 27–August 2","August 3–9","August 10–16","August 17–23","August 24–30"],[
      "Alma 39–42","Alma 43–52","Alma 53–63","Helaman 1–6","Helaman 7–12"
    ]),
    Months("September",[
      "August 31–September 6","September 7–13","September 14–20","September 21–27"],[
      "Helaman 13–16","3 Nephi 1–7","3 Nephi 8–11","3 Nephi 12–16"
    ]),
    Months("October",[
      "September 28–October 11","October 12–18","October 19–25"],[
      "3 Nephi 17–19","3 Nephi 20–26","3 Nephi 27–4 Nephi"
    ]),
    Months("November",[
      "October 26–November 1","November 2–8","November 9–15","November 16–22","November 23–29"],[
      "Mormon 1–6","Mormon 7–9","Ether 1–5","Ether 6–11","Ether 12–15"
    ]),
    Months("December",[
      "November 30–December 6","December 7–13","December 14–20","December 21–27"],[
      "Moroni 1–6","Moroni 7–9","Moroni 10","Christmas"
    ]),
  ];


  static String appName = "Gospel Trivia";
  static int numberOfLevels = 11;
  static String downloadLink = "http://mckayt.me/gospeltrivia/";
  static String sharedPreferencesName = appName.trim().toLowerCase();

  static String shareMessage(int correctAnswers) {
    return "I just played $appName!\n My high score is $correctAnswers points. Can you beat it? Download the app from here:\n $downloadLink";
  }

  static Color hexBackgroundColor = Color(0xFF001436);
  static Color hexButtonsFrameColor = Color(0xFF28657F);
  static Color hexButtonsTextColor = Color(0xFFFFFFFF);
  static Color hexQuestionTextColor = Color(0xFFFFFFFF);
  static Color hexAnswerTrackerColor = Color(0xFF28657F);

  static String logoImage = "assets/images/logo.png";
  static String titleImage = "assets/images/title.png";
}
