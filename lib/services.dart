import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:followme/json_question.dart';

class Services {

  static Future<String> loadQuestionsString(int level) async{
    var jsonText = await rootBundle.loadString("assets/json_questions/$level.json");
    return jsonText;
  }

  static Future<List<JsonQuizQuestion>> getQuestions(int level) async {
      final response = await loadQuestionsString(level);
        List<JsonQuizQuestion> list = parseQuestions(response);
        return list;
  }

  static List<JsonQuizQuestion> parseQuestions(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<JsonQuizQuestion>((json) => JsonQuizQuestion.fromJson(json)).toList();
  }
}