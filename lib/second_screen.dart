import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:followme/app_settings_and_data.dart';
import 'package:followme/first_screen.dart';
import 'package:followme/quiz_bloc/bloc/quiz_level_bloc.dart';
import 'package:followme/quiz_question.dart';
import 'package:followme/services.dart';
import 'custom_expansion_tile.dart' as custom;

import 'json_question.dart';
import 'main.dart';
import 'quiz_bloc/events/quiz_level_event.dart';

class SecondScreen extends StatefulWidget {
  // final List<int> results;
  final int level;

  const SecondScreen({Key key, this.level}) : super(key: key);

  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {

  @override
  void initState() {
   // BlocProvider.of<QuizLevelBloc>(context).add(QuizLevelEvent.change(widget.level),);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(SettingsAndData.appName),
        backgroundColor: SettingsAndData.hexBackgroundColor,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.home,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushReplacement(
                context,
                new MaterialPageRoute(
                  builder: (context) => FirstScreen(),
                ),
              );
            },
          ),
        ],
      ),
      backgroundColor: SettingsAndData.hexBackgroundColor,
      body: SafeArea(
        child: Container(
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "8 points required to advance levels.",
                        style: TextStyle(
                            fontSize: 22,
                            color: SettingsAndData.hexQuestionTextColor),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    //for(int i = 1; i<= SettingsAndData.questionData.length;i++)
                    for (int i = 0; i < SettingsAndData.months.length; i++)
          custom.ExpansionTile(
          headerBackgroundColor: SettingsAndData.hexButtonsFrameColor,
          iconColor: Colors.white,
          title: Container(
            width: double.infinity,
            height: 50,
            color: SettingsAndData.hexButtonsFrameColor,
            child: Center(child: Text(SettingsAndData.months[i].month, style: TextStyle(fontSize: 18.0,color: Colors.white),)),),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton(
                    onPressed: (){},
                    child: Column(
                    children: <Widget>[
                      for (int j = 0; j < SettingsAndData.months[i].monthRange.length; j++)
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                SettingsAndData.months[i].monthRange[j], style: TextStyle(fontSize: 12.0, color: SettingsAndData.hexQuestionTextColor),
                                textAlign: TextAlign.center,  ),
                              Text(
                                SettingsAndData.months[i].monthTitle[j], style: TextStyle(fontSize: 24.0, color: SettingsAndData.hexQuestionTextColor),
                                textAlign: TextAlign.center, ),
                            ],
                          ),
                        )
                    ],
                  ),
                  ),
                ),
              ],
          //getLevel(i, widget.level),
          ),],
                ),
              ),
            ),

        ),),
      );
  }

  Padding getLevel(int level, int levelReached) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: InkWell(
        onTap: () {
          if (levelReached >= level) {
            Services.getQuestions(level).then((value) {
              List<QuizQuestion> questions = [];
              for (int i = 0; i < value[0].questions.length; i++) {
                questions.add(QuizQuestion(
                    value[0].questions[i].questionText,
                    value[0].questions[i].answers,
                    value[0].questions[i].correctAnswer));
              }
              Navigator.pushReplacement(
                context,
                new MaterialPageRoute(
                  builder: (context) => QuizPage(
                      selectedLevel: level,
                      highestUnlockedLevel: levelReached,
                      questions: questions),
                ),
              );
            });
          }
        },
        child: Container(
          width: 220,
          decoration: BoxDecoration(
            border: Border.all(
              color: SettingsAndData.hexButtonsFrameColor,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Text(
                  "Level $level",
                  style: TextStyle(
                      color: SettingsAndData.hexButtonsTextColor,
                      fontSize: 22.0),
                ),
                onPressed: null,
              ),
              IconTheme(
                data: new IconThemeData(
                    color: SettingsAndData.hexButtonsTextColor),
                child: levelReached >= level
                    ? new Icon(Icons.check)
                    : new Icon(Icons.lock),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*
for(int i = 1; i<9;i++)
                    widget.results[i-1]!=null ? getLevel(i, true) : getLevel(i, false)
 */
