import 'dart:async';
import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:followme/first_screen.dart';
import 'package:followme/question.dart';
import 'package:followme/quiz_bloc/bloc/quiz_level_bloc.dart';
import 'package:followme/quiz_bloc/bloc/quiz_level_bloc_delegate.dart';
import 'package:followme/quiz_bloc/events/quiz_level_event.dart';
import 'package:followme/quiz_question.dart';
import 'package:followme/second_screen.dart';
import 'package:followme/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'app_settings_and_data.dart';
import 'json_question.dart';
import 'quiz_brain.dart';

enum Buttons { one, two, three, four }

QuizBrain quizBrain = QuizBrain();
void main() {
  BlocSupervisor.delegate = QuizLevelBlocDelegate();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(Quizzler());
  });
}

class Quizzler extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<QuizLevelBloc>(
      create: (context) => QuizLevelBloc(),
      child:
          MaterialApp(debugShowCheckedModeBanner: false, home: FirstScreen()),
    );
  }
}

class QuizPage extends StatefulWidget {
  final int selectedLevel;
  final int highestUnlockedLevel;
  final List<QuizQuestion> questions;

  const QuizPage(
      {Key key, this.selectedLevel, this.highestUnlockedLevel, this.questions})
      : super(key: key);

  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<Color> answersColor = [
    Colors.transparent,
    Colors.transparent,
    Colors.transparent,
    Colors.transparent,
    Colors.transparent,
    Colors.transparent,
    Colors.transparent,
    Colors.transparent,
    Colors.transparent,
    Colors.transparent
  ];
  int correctAnswersCounter = 0;

  Color buttonOneColor, buttonTwoColor, buttonThreeColor, buttonFourColor;
  bool buttonDisabled=false;


  String dialogTitle;
  String dialogButton;

  Buttons selectedButton;
  String selectedAnswer;


  Timer _timer;
  int _start = 10;
  int scoreSum = 0;
  bool finaLevel = false;

  var buttonIndexes = [1, 2, 3, 4];

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) => setState(
            () {
                if (_start <= 0) {
                  timeIsUp();
                }
                else{
                      _start=_start-1;
                }
        },
      ),
    );
  }

  @override
  void initState() {
    buttonIndexes.shuffle();
    quizBrain.setQuizLevel(widget.selectedLevel);
    quizBrain.setQuestionBank(widget.questions);
    super.initState();
    setButtons();
    Timer(Duration(seconds: 1), () {
      startTimer();
    });
  }

  void timeIsUp(){
    String wrongAnswer = "Wrong answer";
    Buttons selectedButton;
    String correctAnswer = quizBrain.getCorrectAnswer();
    selectedAnswer= correctAnswer;

    if(quizBrain.getSuggestedAnswers()[0] == selectedAnswer){
      selectedButton = Buttons.one;
    }
    else if(quizBrain.getSuggestedAnswers()[1] == selectedAnswer){
      selectedButton = Buttons.two;
    }
    else if(quizBrain.getSuggestedAnswers()[2] == selectedAnswer){
      selectedButton = Buttons.three;
    }
    else if(quizBrain.getSuggestedAnswers()[3] == selectedAnswer){
      selectedButton = Buttons.four;
    }

    checkAnswer(wrongAnswer, selectedButton);
  }

  void callCheckAnswer(){
    if(!buttonDisabled) {
      setState(() {
        buttonDisabled= true;
      });
      checkAnswer(selectedAnswer, selectedButton);
    }
  }

  void checkAnswer(String userPickedAnswer, Buttons button) {
    String correctAnswer = quizBrain.getCorrectAnswer();
    setState(() {
      _timer.cancel();
      int questionNumber = quizBrain.getQuestionNumber();
      if (userPickedAnswer == correctAnswer) {
        answersColor[questionNumber] = Colors.green;
        correctAnswersCounter++;
        if (button == Buttons.one) {
          buttonOneColor = Colors.green;
        } else if (button == Buttons.two) {
          buttonTwoColor = Colors.green;
        } else if (button == Buttons.three) {
          buttonThreeColor = Colors.green;
        } else if (button == Buttons.four) {
          buttonFourColor = Colors.green;
        }
      } else {
        answersColor[questionNumber] = Colors.red;

        if (button == Buttons.one) {
          buttonOneColor = Colors.red;
        } else if (button == Buttons.two) {
          buttonTwoColor = Colors.red;
        } else if (button == Buttons.three) {
          buttonThreeColor = Colors.red;
        } else if (button == Buttons.four) {
          buttonFourColor = Colors.red;
        }

        int correctAnswerIndex = quizBrain.getCorrectAnswerIndex();
        if (correctAnswerIndex == 0) {
          buttonOneColor = Colors.green;
        } else if (correctAnswerIndex == 1) {
          buttonTwoColor = Colors.green;
        } else if (correctAnswerIndex == 2) {
          buttonThreeColor = Colors.green;
        } else if (correctAnswerIndex == 3) {
          buttonFourColor = Colors.green;
        }
      }
      Timer(Duration(seconds: 2), () {
        setState(() {
          quizBrain.nextQuestion();
          setButtons();
          buttonDisabled = false;
          startTimer();
          _start = 10;
        });
      });
      if (quizBrain.isFinished()) {
        saveScore(quizBrain.getQuizLevel(), correctAnswersCounter);

        if (correctAnswersCounter >= 8) {
          if (quizBrain.getQuizLevel() < SettingsAndData.numberOfLevels) {
            dialogTitle = "Congratulations!";
            dialogButton = "Go to the next level";
            finaLevel = false;
            quizBrain.increaseLevel();
          }
          else {
            dialogTitle =
                "Congratulations, you completed all the levels! More levels coming soon! Please submit your score";
            dialogButton = "Submit";
            finaLevel = true;
            for(int i = 1; i<= SettingsAndData.numberOfLevels;i++){
              int levelScore = getScore(i);
              if(levelScore!=null)
                scoreSum+=levelScore;
            }
          }
        }
        else {
          dialogTitle = "Sorry, try again";
          dialogButton = "Try again";
          finaLevel = false;
        }

        Timer(Duration(seconds: 2), () {
          _timer.cancel();
          _showDialog(finaLevel);
          quizBrain.reset();

          answersColor = [
            Colors.transparent,
            Colors.transparent,
            Colors.transparent,
            Colors.transparent,
            Colors.transparent,
            Colors.transparent,
            Colors.transparent,
            Colors.transparent,
            Colors.transparent,
            Colors.transparent
          ];

          setButtons();
          buttonDisabled = false;

          Services.getQuestions(quizBrain.getQuizLevel()).then((value) {
            List<QuizQuestion> questions = [];
            for (int i = 0; i < value[0].questions.length; i++) {
              questions.add(QuizQuestion(
                  value[0].questions[i].questionText,
                  value[0].questions[i].answers,
                  value[0].questions[i].correctAnswer));
            }
            quizBrain.setQuestionBank(questions);
          });
        });
        _start = 10;
      }
    });
    if (quizBrain.getQuizLevel() > widget.highestUnlockedLevel) {
      BlocProvider.of<QuizLevelBloc>(context).add(
        QuizLevelEvent.change(quizBrain.getQuizLevel()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<QuizLevelBloc>(
      create: (context) => QuizLevelBloc(),
      child: WillPopScope(
        onWillPop: (){_onBackPressed(quizBrain.getQuizLevel());},
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => _onBackPressed(quizBrain.getQuizLevel())
            ),
            title: Text(SettingsAndData.appName),
            backgroundColor: SettingsAndData.hexBackgroundColor,
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  FontAwesomeIcons.home,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => FirstScreen(),
                    ),
                  );
                },
              ),
            ],
          ),
          backgroundColor: SettingsAndData.hexBackgroundColor,
          body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 18.0, left: 18.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      scoreTracker(answersColor[0]),
                      scoreTracker(answersColor[1]),
                      scoreTracker(answersColor[2]),
                      scoreTracker(answersColor[3]),
                      scoreTracker(answersColor[4]),
                      scoreTracker(answersColor[5]),
                      scoreTracker(answersColor[6]),
                      scoreTracker(answersColor[7]),
                      scoreTracker(answersColor[8]),
                      scoreTracker(answersColor[9]),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      _start>0?_start.toString():"0",
                      style: TextStyle(
                        fontSize: 25.0,
                        color: SettingsAndData.hexQuestionTextColor,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Center(
                      child: Text(
                        quizBrain.getQuestionText(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 25.0,
                          color: SettingsAndData.hexQuestionTextColor,
                        ),
                      ),
                    ),
                  ),
                ),
                for(int index in buttonIndexes)
                  getAnsweringButtons(index),
                SizedBox(
                  height: 10.0,
                ),
                Divider(color: SettingsAndData.hexButtonsFrameColor),
                SizedBox(
                  height: 10.0,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: SettingsAndData.hexButtonsFrameColor),
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                      ),
                      child: FlatButton(
                        textColor: SettingsAndData.hexButtonsTextColor,
                        child: Text(
                          "RESTART",
                          style: TextStyle(
                            color: SettingsAndData.hexButtonsTextColor,
                            fontSize: 25.0,
                          ),
                        ),
                        onPressed: () {
                          buttonDisabled
                              ? null
                              : setState(() {
                                  answersColor = [
                                    Colors.transparent,
                                    Colors.transparent,
                                    Colors.transparent,
                                    Colors.transparent,
                                    Colors.transparent,
                                    Colors.transparent,
                                    Colors.transparent,
                                    Colors.transparent,
                                    Colors.transparent,
                                    Colors.transparent
                                  ];
                                  quizBrain.reset();
                                  correctAnswersCounter = 0;
                                  _start = 10;
                          });
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed(int level) {
    SecondScreen secondScreen;
    if (level > widget.highestUnlockedLevel) {
      secondScreen = SecondScreen(level: level);
      saveLevel(level);
    }
    else {
      secondScreen = SecondScreen(level: widget.highestUnlockedLevel);
    }
    return Navigator.pushReplacement(
        context,
        new MaterialPageRoute(
          builder: (context) => secondScreen,
        ),) ?? false;
    }

  @override
  void dispose() {
    _timer.cancel();
    answersColor = [
      Colors.transparent,
      Colors.transparent,
      Colors.transparent,
      Colors.transparent,
      Colors.transparent,
      Colors.transparent,
      Colors.transparent,
      Colors.transparent,
      Colors.transparent,
      Colors.transparent
    ];
    quizBrain.reset();
    correctAnswersCounter = 0;
    scoreSum = 0;
    finaLevel = false;
    super.dispose();
  }

  void _showDialog(bool finalLevel) {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(dialogTitle),
          content: new Text(
              "Correct: $correctAnswersCounter \n Incorrect: ${10 - correctAnswersCounter} \n Score: ${correctAnswersCounter * 10}%"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(dialogButton),
              onPressed: () {
                if(finalLevel){
                  callSendEmail(scoreSum.toString());
                  Navigator.of(context).pop();
                  correctAnswersCounter = 0;
                }
                else{
                  Navigator.of(context).pop();
                  correctAnswersCounter = 0;
                  startTimer();
                  Timer(Duration(seconds: 1), () {
                    _start=10;
                  });
                }
                },
            ),
          ],
        );
      },
    );
  }

  void setButtons() {
    buttonOneColor = Colors.transparent;
    buttonTwoColor = Colors.transparent;
    buttonThreeColor = Colors.transparent;
    buttonFourColor = Colors.transparent;
  }

  saveScore(int level, int correct) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // quizler_lvl
    preferences.setInt(
        SettingsAndData.sharedPreferencesName + "s" + level.toString(),
        correct);
  }

  saveLevel(int lvl) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // quizler_lvl
    preferences.setInt(SettingsAndData.sharedPreferencesName + "l", lvl);
  }

  static Padding scoreTracker(Color color) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Container(
        height: 16,
        width: 16,
        decoration: BoxDecoration(
            color: color,
            border: Border.all(color: SettingsAndData.hexAnswerTrackerColor)),
      ),
    );
  }

  void sendEmail(command) async{
    if(await canLaunch(command)){
      await launch(command);
    }
  }

  void callSendEmail(String score){
    var date = new DateTime.now();
    var dateValues = new DateTime(date.year, date.month, date.day, date.hour, date.minute);
    print(dateValues);
    print(dateValues.toString());
    String month = date.month.toString();
    if(month=="01"){
      month = "January";
    }
    else if(month=="02"){
      month = "February";
    }
    else if(month=="03"){
      month = "March";
    }
    else if(month=="04"){
      month = "April";
    }
    else if(month=="05"){
      month = "May";
    }
    else if(month=="06"){
      month = "Juny";
    }
    else if(month=="07"){
      month = "July";
    }
    else if(month=="08"){
      month = "August";
    }
    else if(month=="09"){
      month = "September";
    }
    else if(month=="10"){
      month = "October";
    }
    else if(month=="11"){
      month = "November";
    }
    else if(month=="12"){
      month = "December";
    }
    String emailTitle = "Gospel Trivia Submission";
    String body = "${date.year}.${date.month}.${date.day}.${date.hour}.${date.minute} <br> Score:${scoreSum.toString()} <br><br> Please enter the username you want to be displayed on the leader board here. <br> Username:";
    sendEmail('mailto:mckay.thomason@gmail.com?subject=$emailTitle&body=$body');
  }

  getScore(int lvl) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // int lvl = preferences.getInt("scorelevelcc");
    int score = preferences.getInt(SettingsAndData.sharedPreferencesName +"s" + lvl.toString());
    return score;
  }


  getAnsweringButtonOne() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
                color: SettingsAndData.hexButtonsFrameColor),
            color: buttonOneColor,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: FlatButton(
            textColor: SettingsAndData.hexButtonsTextColor,
            child: AutoSizeText(
              quizBrain.getSuggestedAnswers()[0],
              style: TextStyle(
                color: SettingsAndData.hexButtonsTextColor,
                fontSize: 20.0,
              ),
            ),
            onPressed:
            buttonDisabled
                ? null
                : () {
              setState(() {
                selectedButton = Buttons.one;
                selectedAnswer = quizBrain.getSuggestedAnswers()[0];
              });
              callCheckAnswer();
            },
          ),
        ),
      ),
    );
  }

  getAnsweringButtonTwo() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
                color: SettingsAndData.hexButtonsFrameColor),
            color: buttonTwoColor,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: FlatButton(
            textColor: SettingsAndData.hexButtonsTextColor,
            child: AutoSizeText(
              quizBrain.getSuggestedAnswers()[1],
              style: TextStyle(
                color: SettingsAndData.hexButtonsTextColor,
                fontSize: 20.0,
              ),
            ),
            onPressed: buttonDisabled
                ? null
                : () {
              setState(() {
                selectedButton = Buttons.two;
                selectedAnswer = quizBrain.getSuggestedAnswers()[1];
              });
              callCheckAnswer();
            },
          ),
        ),
      ),
    );
  }

  getAnsweringButtonThree() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
                color: SettingsAndData.hexButtonsFrameColor),
            color: buttonThreeColor,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: FlatButton(
            textColor: SettingsAndData.hexButtonsTextColor,
            child: AutoSizeText(
              quizBrain.getSuggestedAnswers()[2],
              style: TextStyle(
                color: SettingsAndData.hexButtonsTextColor,
                fontSize: 20.0,
              ),
            ),
            onPressed: buttonDisabled
                ? null
                : () {
              setState(() {
                selectedButton = Buttons.three;
                selectedAnswer = quizBrain.getSuggestedAnswers()[2];
              });
              callCheckAnswer();
            },
          ),
        ),
      ),
    );
  }

  getAnsweringButtonFour() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
                color: SettingsAndData.hexButtonsFrameColor),
            color: buttonFourColor,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: FlatButton(
            textColor: SettingsAndData.hexButtonsTextColor,
            child: AutoSizeText(
              quizBrain.getSuggestedAnswers()[3],
              style: TextStyle(
                color: SettingsAndData.hexButtonsTextColor,
                fontSize: 20.0,
              ),
            ),
            onPressed:
            buttonDisabled
                ? null
                :
                () {
              setState(() {
                selectedButton = Buttons.four;
                selectedAnswer = quizBrain.getSuggestedAnswers()[3];
              });
              callCheckAnswer();
            },
          ),
        ),
      ),
    );
  }


  getAnsweringButtons(int number) {
    switch (number) {
      case 1:
        print("1");
        return getAnsweringButtonOne();
        break;
      case 2:
        print("2");
        return getAnsweringButtonTwo();
        break;
      case 3:
        print("3");
        return getAnsweringButtonThree();
        break;
      case 4:
        print("4");
        return getAnsweringButtonFour();
        break;
      default:
        break;
    }
  }
}
