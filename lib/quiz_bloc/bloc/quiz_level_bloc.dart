import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:followme/quiz_bloc/events/quiz_level_event.dart';

class QuizLevelBloc extends Bloc<QuizLevelEvent, int>{

  @override
  int get initialState => 1;

  @override
  Stream<int> mapEventToState(QuizLevelEvent event) async*{
    int newLevel = state;
    if (event != null) {
      newLevel = event.level;
    }
    yield newLevel;
  }

}