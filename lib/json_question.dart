
class JsonQuizQuestion {
  List questions;
  JsonQuizQuestion({this.questions});

  /* JsonQuizQuestion.fromJson(Map<String, dynamic> json){
    questionText = json['questionText'];
    answers = json['answers'];
    correctAnswer = json['correctAnswer'];
  }
  */

  factory JsonQuizQuestion.fromJson(Map <String, dynamic> json){

    return JsonQuizQuestion(
        questions: parseQuestions(json)
    );
  }

  static List<Question> parseQuestions(questionsJson){
    var list = questionsJson['Questions'] as List;
    List<Question> questionList = list.map((e) => Question.fromJson(e)).toList();
    return questionList;
  }

}

class Question {
  String questionText;
  List<String> answers;
  String correctAnswer;
  Question({this.questionText, this.answers, this.correctAnswer});

  factory Question.fromJson(Map <String, dynamic> json){

    return Question(
        questionText: json['questionText'],
        answers:json['answers'].cast<String>(),
        //   answers: parseAnswers(json['answers']),
        correctAnswer: json['correctAnswer']
    );
  }
}