import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:followme/app_settings_and_data.dart';
import 'package:followme/main.dart';
import 'package:followme/quiz_bloc/bloc/quiz_level_bloc.dart';
import 'package:followme/quiz_bloc/events/quiz_level_event.dart';
import 'package:followme/quiz_brain.dart';
import 'package:followme/scores_screen.dart';
import 'package:followme/second_screen.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' show Platform;

class FirstScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SettingsAndData.hexButtonsFrameColor,
      body: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(38.0),
                  child: Image.asset("assets/images/logotwo.png"),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:20, right: 20),
                  child: SizedBox(
                    height: 50,
                    width: double.infinity,
                    child:
                    InkWell(
                      onTap: () async {
                        int level = await getLevel();
                        print(level);
                        Navigator.push(context, new MaterialPageRoute(builder: (context)=>SecondScreen(level: level,),),);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.white,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child: Center(child: Text("Start", style: TextStyle(fontSize:20, color: SettingsAndData.hexButtonsTextColor),textAlign: TextAlign.center,)),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.only(left:20, right: 20),
                  child: SizedBox(
                    height: 50,
                    width: double.infinity,
                    child:
                    InkWell(
                      onTap: () async {
                        int sum = 0;
                        for(int i = 1; i<= SettingsAndData.numberOfLevels;i++){
                          int levelScore = await getScore(i);
                          if(levelScore!=null)
                          sum+=levelScore;
                        }
                        if (Platform.isAndroid) {
                          Share.share(SettingsAndData.shareMessage(sum), subject: 'Play ${SettingsAndData.appName}!');
                        } else if (Platform.isIOS) {
                          Share.share(SettingsAndData.shareMessage(sum), subject: 'Play ${SettingsAndData.appName}!');
                        }
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.white,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(30))
                          ),
                          child: Center(child: Text("Challenge your \nfriends", style: TextStyle(color: SettingsAndData.hexButtonsTextColor),textAlign: TextAlign.center,)),
                      ),
                    ),

                  ),
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.only(left:20, right: 20),
                  child: SizedBox(
                    height: 50,
                    width: double.infinity,
                    child:
                    InkWell(
                      onTap: ()  async{
                        //  int score = await getScore();
                        // int level = await getLevel();

                        List<int> results = [];
                        int sum = 0;
                        for(int i = 1; i<= SettingsAndData.numberOfLevels;i++){
                          int levelScore = await getScore(i);
                          results.add(levelScore);
                          if(levelScore!=null)
                            sum+=levelScore;
                        }

                        //if(score == null)
                        // Navigator.push(context, new MaterialPageRoute(builder: (context)=>ScoresPage(),),);
                        // else
                        Navigator.push(context, new MaterialPageRoute(builder: (context)=>ScoresPage(results:results, sum:sum),),);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.white,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child: Center(child: Text("STATS", style: TextStyle(fontSize:20, color: SettingsAndData.hexButtonsTextColor),textAlign: TextAlign.center,)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }
}


getScore(int lvl) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
 // int lvl = preferences.getInt("scorelevelcc");
  int score = preferences.getInt(SettingsAndData.sharedPreferencesName +"s" + lvl.toString());
  return score;
}

getLevel() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  int lvl = preferences.getInt(SettingsAndData.sharedPreferencesName +"l");
  if(lvl == null)
    lvl = 1;
  return lvl;
}