import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'app_settings_and_data.dart';
import 'first_screen.dart';

class ScoresPage extends StatefulWidget {
  //final int score;
  //final int level;
  final List<int> results;
  final int sum;

  const ScoresPage({Key key, this.results, this.sum}) : super(key: key);

  @override
  _ScoresPageState createState() => _ScoresPageState();
}

class _ScoresPageState extends State<ScoresPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gospel Trivia'),
        backgroundColor: SettingsAndData.hexBackgroundColor,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.home,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushReplacement(
                context,
                new MaterialPageRoute(
                  builder: (context) => FirstScreen(),
                ),
              );
            },
          ),
        ],
      ),
      backgroundColor: SettingsAndData.hexBackgroundColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(28.0),
                  child: Text(
                    "Scores",
                    style: TextStyle(
                        color: SettingsAndData.hexQuestionTextColor,
                        fontSize: 38.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              for (int i = 0; i < widget.results.length; i++)
                widget.results[i] == null
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Level ${i + 1}:    LOCKED",
                          style: TextStyle(
                              color: SettingsAndData.hexButtonsTextColor,
                              fontSize: 22.0),
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Level ${i + 1}:    ${widget.results[i]}",
                          style: TextStyle(
                              color: SettingsAndData.hexButtonsTextColor,
                              fontSize: 22.0),
                        ),
                      ),
              SizedBox(
                height: 10.0,
              ),
              Divider(color: SettingsAndData.hexButtonsFrameColor),
              SizedBox(
                height: 10.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Total score: ${widget.sum}",
                  style: TextStyle(
                      fontSize: 22,
                      color: SettingsAndData.hexQuestionTextColor),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
