class QuizQuestion {
  String questionText;
  List<String> answers;
  String correctAnswer;

  QuizQuestion(this.questionText, this.answers, this.correctAnswer);

}
