import 'package:followme/quiz_question.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_settings_and_data.dart';

class QuizBrain {
  int quizLevel = 1;
  int correctAnswersCounter = 0;
  int _questionNumber = 0;

  List<QuizQuestion> _questionBank = [];

  void setQuestionBank(List<QuizQuestion> questions) {
/*
    if (quizLevel == 1)
      _questionBank = LvlOneQuestions.questionBank;
    else if (quizLevel == 2)
      _questionBank = LvlTwoQuestions.questionBank;
    else if (quizLevel == 3)
      _questionBank = LvlThreeQuestions.questionBank;
    else if (quizLevel == 4)
      _questionBank = LvlFourQuestions.questionBank;
    else if (quizLevel == 5)
      _questionBank = LvlFiveQuestions.questionBank;
    else if (quizLevel == 6)
      _questionBank = LvlSixQuestions.questionBank;
    else if (quizLevel == 7)
      _questionBank = LvlSevenQuestions.questionBank;
    else if (quizLevel == 8)
      _questionBank = LvlEightQuestions.questionBank;
    else if (quizLevel == 9)
      _questionBank = LvlNineQuestions.questionBank;
    else if (quizLevel == 10) _questionBank = LvlTenQuestions.questionBank;
    */
    _questionBank = questions;
  }

  void nextQuestion() {
    if (_questionNumber < _questionBank.length - 1) {
      _questionNumber++;
    }
  }

  String getQuestionText() {
    return _questionBank[_questionNumber].questionText;
  }

  List<String> getSuggestedAnswers() {
    return _questionBank[_questionNumber].answers;
  }

  String getCorrectAnswer() {
    return _questionBank[_questionNumber].correctAnswer;
  }

  int getCorrectAnswerIndex() {
    for (int i = 0; i < _questionBank[_questionNumber].answers.length; i++) {
      if (_questionBank[_questionNumber].answers[i] ==
          _questionBank[_questionNumber].correctAnswer) return i;
    }
    return null;
  }

  bool isFinished() {
    if (_questionNumber == _questionBank.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  void reset() {
    _questionNumber = 0;
    correctAnswersCounter = 0;
  }

  void increaseCorrectAnswer() {
    correctAnswersCounter++;
  }

  int getCorrectAnswersCount() {
    return correctAnswersCounter;
  }

  int getQuestionNumber () {
    return _questionNumber;
  }

  void increaseLevel() {
    quizLevel++;
    //saveQuizLvl(quizLevel);
  }

  int getQuizLevel() {
    return quizLevel;
  }

  void setQuizLevel(int level){
    quizLevel = level;
  }

  void restartQuizLevel() {
    quizLevel = 1;
    saveQuizLvl(quizLevel);
  }

  saveQuizLvl(int lvl) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt(SettingsAndData.sharedPreferencesName +"l", lvl);
  }

}
